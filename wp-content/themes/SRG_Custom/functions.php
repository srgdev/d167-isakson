<?php
/*----------------------------------------------------------------------------------------------------
* StoneRidgeGroup Function File for WordPress
* Copyright 2014
* By: Calvin deClaisse-Walford
----------------------------------------------------------------------------------------------------*/

/**
 * Do not allow remote editing of files
 */
define( 'DISALLOW_FILE_EDIT', true );


require_once TEMPLATEPATH . '/includes/options.php';

show_admin_bar( false );
remove_action( 'wp_head', 'jetpack_og_tags' );
add_filter('jetpack_enable_open_graph', '__return_false', 99);

// Include JQuery and other scripts
function srg_enqueue_scripts(){
    // If not admin, add latest jQuery to frontend page headers
    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.3.2', false);
        wp_enqueue_script('jquery');
    }

    // Enque all our custom scripts and plugins
//    wp_enqueue_script( 'popup', get_template_directory_uri() . '/popup/jquery.popup.js', array('jquery'), false, true );
    wp_enqueue_script( 'twitter', ("//platform.twitter.com/widgets.js"), null, false, true);
    wp_enqueue_script( 'srg_main', get_template_directory_uri() . '/js/min/main-min.js', array('jquery'), false, true );

    // Pass a global javascript object that contains the correct URL to process AJAX requests
    wp_localize_script('srg_main', 'SRG_Ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' ), 'siteurl' => get_bloginfo('url'), 'thankpage' => get_permalink(get_srg_option('srg_page_thank'))));
    wp_localize_script('srg_main', 'SRG_Splash', array('splash_page_url' => get_splash(), 'session_seen_flash' => get_session_seen_flash(), 'session_key' => get_session_key()));
}

add_action('wp_enqueue_scripts', 'srg_enqueue_scripts');

/*----------------------------------------------------------------------------------------------------*/

/**
 * Installs tables for forms
 */
function srg_install_tables(){

    if(!get_option('srg_cc_donations_table_installed_1')){
        global $wpdb;
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // Contact Form Table
        $table_name = $wpdb->prefix . 'cc_donations';
        $sql = "CREATE TABLE $table_name (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `name` varchar(255) DEFAULT NULL,
            `email` varchar(255) DEFAULT NULL,
            `date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
            `amount` varchar(255),
            `type` varchar(255),
            `expires` varchar(255),
            `transaction_id` varchar(255),
            `user_agent` varchar(255),
            `donate_res` varchar(255),
            `i_res` varchar(255),
            PRIMARY KEY (`id`)
            );";

        dbDelta( $sql );

        update_option('srg_cc_donations_table_installed_1', true);

    }


}
add_action("init", "srg_install_tables");

/*----------------------------------------------------------------------------------------------------*/

if(function_exists('add_db_table_editor')){

    add_db_table_editor(array( 'title'=>'Donations', 'table'=>'wp_cc_donations'));

}

/*----------------------------------------------------------------------------------------------------*/

function get_splash(){
    return get_theme_mod('splash_page') == 0 ? false : get_permalink(get_theme_mod('splash_page'));
}

function get_session_seen_flash(){
    return @$_SESSION['seen_splash_'.get_theme_mod('splash_page')] ? true : false;
}

function get_session_key(){
    return 'seen_splash_'.get_theme_mod('splash_page');
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Load packaged plugins
 * @uses srg_custom_fields()
 * @hook after_setup_theme
 */
function srg_plugin_loader(){
    include(TEMPLATEPATH . '/includes/plugins/advanced-custom-fields/acf.php');
    include(TEMPLATEPATH . '/includes/plugins/acf-repeater/acf-repeater.php');

}
add_action( 'after_setup_theme', 'srg_plugin_loader' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 */
 if ( ! function_exists( 'srg_setup' ) ):
function srg_setup() {

    // This theme uses post thumbnails
    add_theme_support( 'post-thumbnails' );

    // Set up some default thumbnail size
    $thumbnail_size = array();
    $thumbnail_size['width'] = 387;
    $thumbnail_size['height'] = 257;
    add_image_size( 'srg-default', $thumbnail_size['width'], $thumbnail_size['height'], true );

    // Add default posts and comments RSS feed links to head
    add_theme_support( 'automatic-feed-links' );

    // This theme uses wp_nav_menu() in one location
    register_nav_menus( array('header' => 'Main Menu') );
    register_nav_menus( array('footer' => 'Footer Menu') );

    // Make theme available for translation
    // Translations can be filed in the /languages/ directory
    load_theme_textdomain( 'srg', TEMPLATEPATH . '/languages' );

    $locale = get_locale();
    $locale_file = TEMPLATEPATH . "/languages/$locale.php";
    if ( is_readable( $locale_file ) )
        require_once( $locale_file );

}
endif;

add_action( 'after_setup_theme', 'srg_setup' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Makes some changes to the <title> tag, by filtering the output of wp_title().
 *
 * @param string $title Title generated by wp_title()
 * @param string $separator The separator passed to wp_title().
 * @return string The new title, ready for the <title> tag.
 */
function srg_filter_wp_title( $title, $separator ) {

    // Don't affect wp_title() calls in feeds.
    if ( is_feed() )
        return $title;

    // The $paged global variable contains the page number of a listing of posts.
    // The $page global variable contains the page number of a single post that is paged.
    // We'll display whichever one applies, if we're not looking at the first page.
    global $paged, $page;

    if ( is_search() ) {
        // If we're a search, let's start over:
        $title = sprintf( 'Search results for %s', '"' . get_search_query() . '"' );
        // Add a page number if we're on page 2 or more:
        if ( $paged >= 2 )
            $title .= " $separator " . sprintf( 'Page %s', $paged );
        // Add the site name to the end:
        $title .= " $separator " . get_bloginfo( 'name', 'display' );
        // We're done. Let's send the new title back to wp_title():
        return $title;
    }

    // Otherwise, let's start by adding the site name to the end:
    $title .= get_bloginfo( 'name', 'display' );

    // If we have a site description and we're on the home/front page, add the description:
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title .= " $separator " . $site_description;

    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 )
        $title .= " $separator " . sprintf( 'Page %s', max( $paged, $page ) );

    // Return the new title to wp_title():
    return $title;
}
add_filter( 'wp_title', 'srg_filter_wp_title', 10, 2 );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Sets the post excerpt length to 80 characters.
 *
 * @return int
 */
function srg_excerpt_length( $length ) {
   return 50;
}
add_filter( 'excerpt_length', 'srg_excerpt_length' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @return string "Continue Reading" link
 */
function srg_continue_reading_link() {
    return '... <a href="'.get_permalink().'" class="red_text">Read more</a>';
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and srg_continue_reading_link().
 *
 * @return string An ellipsis
 */
function srg_auto_excerpt_more( $more ) {
    return srg_continue_reading_link();
}
add_filter( 'excerpt_more', 'srg_auto_excerpt_more' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function srg_custom_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= srg_continue_reading_link();
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'srg_custom_excerpt_more' );

/*----------------------------------------------------------------------------------------------------*/

// Add post_type parameter to the wp_get_archives() function
function srg_custom_post_type_archive_where($where,$args){
    $post_type  = isset($args['post_type'])  ? $args['post_type']  : 'post';
    $where = "WHERE post_type = '$post_type' AND post_status = 'publish'";
    return $where;
}

add_filter( 'getarchives_where','srg_custom_post_type_archive_where',10,2);

/*----------------------------------------------------------------------------------------------------*/

/**
* Define a simple function to output our menu cleanly, adding donate links / logos if their settings have been set in backend
*
* @param string $location
* @uses wp_nav_menu()
*/
function srg_nav_menu($location = '') {

    $ulid = $location == 'header' ? 'top_navigation' : 'bottom_navigation';
    $items_wrap = '<ul class='.$ulid.'>';
    $items_wrap .= '%3$s';
    $items_wrap .= '</ul>';

    $args = array(
        'theme_location'  => $location,
        'menu'            => '',
        'container'       => false,
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => '',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => $items_wrap,
        'depth'           => 0,
        'walker'          => ''
    );

    wp_nav_menu( $args );

};

/*----------------------------------------------------------------------------------------------------*/

/**
 * Add some custom options to the customize panel
 *
 *@uses $wp_customize object for setting Theme Options panel sections
 *@uses add_setting()
 *@uses add_section()
 *@uses add_control()
 */
function srg_customize_register($wp_customize) {

    require_once(TEMPLATEPATH . '/includes/classes/WP_Customize_Control_Textarea.php');

    // Add settings from registered options
    $wp_customize->add_setting( 'facebook', array('default' => '','type' => 'theme_mod') );
    $wp_customize->add_setting( 'twitter', array('default' => '','type' => 'theme_mod') );
	$wp_customize->add_setting( 'youtube', array('default' => '','type' => 'theme_mod') );
    $wp_customize->add_setting( 'donate', array('default' => '','type' => 'theme_mod') );
    $wp_customize->add_setting( 'presskit_file', array('default' => '','type' => 'theme_mod') );
    $wp_customize->add_setting( 'splash_page', array('default' => '','type' => 'theme_mod') );

    // Analytics
    $wp_customize->add_setting( 'srg_theme_analytics', array('default' => '','type' => 'theme_mod') );

    // Add options sections
    $wp_customize->add_section( 'srg_custom', array('title' => 'SRG Theme Options','priority' => 31) );

    // Add controls to our options

    // Custom opts
    $wp_customize->add_control( 'facebook', array('label' => __( 'Facebook Link' ),'section' => 'srg_custom',) );
    $wp_customize->add_control( 'twitter', array('label' => __( 'Twitter Link' ),'section' => 'srg_custom',) );
	$wp_customize->add_control( 'youtube', array('label' => __( 'YouTube Link' ),'section' => 'srg_custom',) );
    $wp_customize->add_control( 'donate', array('label' => __( 'Donate Link' ),'section' => 'srg_custom',) );
    $wp_customize->add_control( 'presskit_file', array('label' => __( 'PressKit File Link' ),'section' => 'srg_custom', ) );
    $wp_customize->add_control( 'splash_page', array('label' => __( 'Select a splash page' ),'section' => 'srg_custom', 'type' => 'dropdown-pages',)  );
    // Analytics
    $wp_customize->add_control( new WP_Customize_Control_Textarea( $wp_customize, 'srg_theme_analytics', array('label' => __( 'Google Analytics Code'), 'section' => 'srg_custom', 'settings' => 'srg_theme_analytics')));
}

add_action( 'customize_register', 'srg_customize_register' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Add analytics code to Footer
 * @uses get_option() to retrieve custom set color options
 */

function srg_footer_analytics(){
    echo get_theme_mod('srg_theme_analytics');
}
add_filter('wp_footer', 'srg_footer_analytics');

/*----------------------------------------------------------------------------------------------------*/

/**
 * Safe fallback for showing splash page when amplify doesnt want to cooperate
 */
function redirect_to_splash(){
    if(is_home()){
        if(get_splash()){
            if (!session_id()){
                session_start();
            }

            if(!isset($_SESSION['seen_splash_'.get_theme_mod('splash_page')])){
                $_SESSION['seen_splash_'.get_theme_mod('splash_page')] = true;
                wp_redirect(get_permalink(get_theme_mod('splash_page')));
                exit();
            }
        }
    }
    
}

add_action('init', 'redirect_to_splash', 1);

/*----------------------------------------------------------------------------------------------------*/

/**
 * Process donate form information on frontend - adds a nopriv function call and a frontend localization for our main JS
 * @uses $_POST global
 */
function srg_donate() {
    // parse $_POST array
    $data = $_POST;

    // Set up return
    $return = false;

    // Verify nonce
    if(wp_verify_nonce($data['_wpnonce'], 'srg_donate')){

        // Unset nonce field and others
        unset($data['_wpnonce'], $data['_wp_http_referer'], $data['action']);

        // Secure the information
        // Clean the input
        foreach($data as $key=>$val){
            if($key != 'email'){
                $data[$key] = sanitize_text_field($val);
                $data[$key] = str_replace('"', '', $val);
                $data[$key] = str_replace("'", '', $val);
            } else {
                $data[$key] = sanitize_email($val);
            }
        }

        // Verify submitted information

        // Amount
        if(!isset($data['amount']) || !is_numeric($data['amount'])){
            $return = array('code' => 402, 'message' => 'Amount incorrect or not numeric.');
            ajax_respond($return);
        }

        // Frequency must be 'O' or 'M'
        if((!isset($data['frequency'])) || (strlen($data['frequency']) != 1)){
            $data['frequency'] = 'O';
        }

        // Names
        if(!isset($data['fname']) || !isset($data['lname'])){
            $return = array('code' => 402, 'message' => 'Name information missing.');
            ajax_respond($return);
        }

        // String parts of address
        if(!isset($data['address1']) || !isset($data['city'])){
            $return = array('code' => 402, 'message' => 'Street address or City information missing.');
            ajax_respond($return);
        }

        // Email
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $return = array('code' => 402, 'message' => 'Invalid Email.');
            ajax_respond($return);
        }

        // Phone
        if(!preg_match("/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/", $data['phone'])){
            $return = array('code' => 402, 'message' => 'Invalid Phone.');
            ajax_respond($return);
        }

        // State abbreviation
        if(!preg_match("/^[A-Z][A-Z]$/", $data['state'])){
            $return = array('code' => 402, 'message' => 'Invalid State abbreviation.');
            ajax_respond($return);
        }

        // ZIP code
        if(!preg_match("/^\d{5}$/", $data['zip'])){
            $return = array('code' => 402, 'message' => 'Invalid ZIP code.');
            ajax_respond($return);
        }

        // Credit Card
        if(!preg_match("/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|6(?:011|5[0-9]{2})[0-9]{12})$/", $data['ccnum'])){
            $return = array('code' => 402, 'message' => 'Invalid Credit Card number.');
            ajax_respond($return);
        }

        // CVC
        if(!preg_match("/^[0-9]{3,4}$/", $data['cvc'])){
            $return = array('code' => 402, 'message' => 'Invalid Credit Card Security Code.');
            ajax_respond($return);
        }

        // Expiration month & year
        if(!isset($data['exp_month']) || !is_numeric($data['exp_month']) || ($data['exp_month'] > 12 || $data['exp_month'] < 1)){
            $return = array('code' => 402, 'message' => 'Invalid Expiration Month.');
            ajax_respond($return);
        }
        if(!isset($data['exp_year']) || !is_numeric($data['exp_year']) || ($data['exp_year'] < date('Y'))){
            $return = array('code' => 402, 'message' => 'Invalid Expiration Year.');
            ajax_respond($return);
        }

        // Names
        if(!isset($data['occupation']) || !isset($data['employer']) || trim($data['employer'] == '' || trim($data['occupation'] == ''))){
            $return = array('code' => 402, 'message' => 'Name information missing.');
            ajax_respond($return);
        }

        // Save a record before we begin the transaction
        $local_id = create_initial_database_entry($data);

        // Prep for donation
        $url = 'https://www.campaigncontribution.com/api/payment.asp';
        $args = array(
            'body' => array(
                //'Test' => 'Y', //Testing for now
                'KeyCode' => 'FB2B443E4CBA4CCBB97FA5EFF59850027211',
                'SiteCode' => 'FEC12974-624C-4DDE-A212-85A98795AA84',
                'FirstName' => $data['fname'],
                'LastName' => $data['lname'],
                'Address1Line1' => $data['address1'],
                'Address1Line2' => $data['address2'],
                'Address1City' => $data['city'],
                'Address1StateCode' => $data['state'],
                'Address1Postal' => $data['zip'],
                'Email' => $data['email'],
                'Occupation' => $data['occupation'],
                'Employer' => $data['employer'],
                'Amount' => $data['amount'], //in dollars
                'RecurFrequencyCode' => $data['frequency'], //O = one time, M = monthly, Y = yearly
                'CardNumber' => $data['ccnum'], // CC number
                'CardExpirationMonth' => $data['exp_month'], //MM
                'CardExpirationYear' => $data['exp_year'], // YYYY
                'CardCVV' => $data['cvc'], //CVV
                'CDV1' => (isset($data['source']) ? $data['source'] : 'Organic') //Soure Code
            )
        );

        // We will assume 1 year for recurring
        // @TODO check recurring length?
        if($data['frequency'] == 'M'){

            $args['body']['RecurExpirationMonth'] = '10';
            $args['body']['RecurExpirationYear'] = '2016';

        }

        // Send request
        $res_req = wp_remote_post($url, $args);

        // Check for errors
        if(is_wp_error($res_req)){

            // Save data here for debugging
            update_entry_with_cc_res(array('tid' => print_r($args['body'], true)), $res_req->get_error_message(), $local_id);

            // Parse some data before saving error
            $cc_length = strlen($args['body']['CardNumber']);
            $args['body']['CardNumber'] = $cc_length;

            // Log error
            error_log($res_req->get_error_message());
            error_log(print_r($args['body'], true));

            // return message
            $return = array('code' => 500, 'message' => 'There was an error processing the credit card information.  Please check the information and try again.');
            ajax_respond($return);
        }

        // Parse response
        $res_query_string = $res_req['body'];
        $res = explode('&', $res_query_string);

        // Set up our response array container
        $response_array = array();

        // Parse through the string response
        foreach($res as $keyval){
            $val_arr = explode('=', $keyval);
            $response_array[$val_arr[0]] = $val_arr[1];
        }

        // Save data here for debugging
        update_entry_with_cc_res($response_array, $res_query_string, $local_id);
    
        // If successful, errorcode will be 0
        if(@$response_array['errorcode'] == '0'){

            // Send to i-360
            // Base64 Encode the user id
            $i360_user_id = '5f44c291-edad-4e51-9c8a-96cd04b419a2';
            //$i360_auth = base64_encode($i360_user_id . ':');
            $i360_auth = 'Authorization: Basic NWY0NGMyOTEtZWRhZC00ZTUxLTljOGEtOTZjZDA0YjQxOWEyOg==';

            $i360_url = 'https://api-platform.i-360.com/1.1/Contacts';

            // API requires JSON arrays of objects

            // Tag Object
            $i360_tags = new stdClass();
            $i360_tags->Key = 'Donation Source';
            if(isset($data['source'])){
                $i360_tags->Value = $data['source'];
            } else {
                $i360_tags->Value = 'Organic';
            }


            // Donation Object
            $i360_donations = new stdClass();
            $i360_donations->DonationType = 'Credit Card';
            $i360_donations->Amount = $data['amount'];

            // Contact Object
            $i360_contact = new stdClass();
            $i360_contact->FirstName = $data['fname'];
            $i360_contact->LastName = $data['lname'];
            $i360_contact->Phone = $data['phone'];
            $i360_contact->Email = $data['email'];
            $i360_contact->Address1 = $data['address1'];
            $i360_contact->Address2 = $data['address2'];
            $i360_contact->City = $data['city'];
            $i360_contact->State = $data['state'];
            $i360_contact->Zip = $data['zip'];
            $i360_contact->Employer = $data['employer'];
            $i360_contact->Occupation = $data['occupation'];
            $i360_contact->RegisteredState = $data['state'];

            // Array of Objects
            $i360_contact->Tags = array($i360_tags);
            $i360_contact->Donations = array($i360_donations);

            // Array of Contact Objects
            $i360_request = new stdClass();
            $i360_request->Contacts = array($i360_contact);

            $i360_request = json_encode($i360_request);

            $header = array(
                $i360_auth,
                'Content-Type: application/json',
                'Content-Length: ' . strlen($i360_request)
            );
            $ch = curl_init($i360_url);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            //curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$i360_request);

            $response = curl_exec($ch);
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);

            // Don't send sensitive data!
            unset($data['ccnum'], $data['cvc'], $data['exp_month'], $data['exp_year']);

            // If it failed, we need to do something!
            if(!$response){
                $response = $http_status;
                $message = 'HTTP Error: ' . $http_status;
                mail_notification('stephanie@johnnyisakson.com', 'A new donation was recorded but not saved to i360', $data, $http_status);

            } else {

                $message = $response;
                $response = json_decode($response);

                // Save entry with response
                update_entry_with_i_res($message, $local_id);

                // Again, check for failures
                if(@$response->Id == ''){
                    mail_notification('stephanie@johnnyisakson.com', 'A new donation was recorded but not saved to i360', $data, $message);
                }
            }

            // Send message back
            $return = array('code' => 200, 'message'=> $message);


        } else {
            // We have access to an error description to return
            $return = array('code' => 400, 'message' => 'There was an error processing your credit card information: '.$response_array['errordesc']);
        }


    } else {
        $return = array('code' => 401, 'message' => 'You are not authorized to do this.');

    }

    ajax_respond($return);
}
add_action('wp_ajax_srg_donate', 'srg_donate');
add_action( 'wp_ajax_nopriv_srg_donate', 'srg_donate' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Send a json message then die
 * @param $message
 */
function ajax_respond($message){
    // Print a response
    echo json_encode($message);

    // Stop processing now
    die(0);
    exit;
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Use WP mail to send a message
 * @param $to
 * @param $subject
 * @param $donation_data
 * @param bool $status
 */
function mail_notification($to, $subject, $donation_data, $status = false){

    $headers = "From: " . 'donations@johnnyisakson.com' . "\r\n";
    $headers .= "Reply-To: ". 'donations@johnnyisakson.com' . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    $message =  '<html><body>';
    $message .= '<h2>A new donation for Johnny Isakson was recorded with Campaign Contribution, but was unable to be sent to i-360.</h2>';

    if($status){
        $message .= 'Status Code or Error Message: ' . $status;
    }

    $message .= '<hr>';
    $message .= '<h2>Donation Information: </h2>';

    foreach($donation_data as $key=>$item){
        if($key == 'frequency'){
            $message .= '<b>frequency</b>: '.($item == 'O' ? 'One Time' : 'Monthly').'<br>';
        } else {
            $message .= '<b>'.$key.'</b>: '.$item.'<br>';
        }

    }

    $message .=  '</body></html>';

    wp_mail($to, $subject, $message, $headers);

}

/*----------------------------------------------------------------------------------------------------*/

function create_initial_database_entry($data){
     // Save to database
    global $wpdb;
    $table_name = $wpdb->prefix . 'cc_donations';

    $data_to_insert =  array(
        'name' => $data['fname'].' '.$data['lname'],
        'email' => $data['email'],
        'amount' => $data['amount'],
        'type' => ($data['frequency'] == 'O' ? 'One-time' : 'Monthly'),
        'user_agent' => $_SERVER['HTTP_USER_AGENT']
    );

    if($data['frequency'] == 'M'){
        $data_to_insert['expires'] = '10/2016';
    }

    $wpdb->insert($table_name, $data_to_insert);    

    return $wpdb->insert_id;
}

function update_entry_with_cc_res($response_array, $res_query_string, $local_id){
    // Save to database
    global $wpdb;
    $table_name = $wpdb->prefix . 'cc_donations';

    $data_to_update =  array(
        'transaction_id' => $response_array['tid'],
        'donate_res' => $res_query_string
    );

    $wpdb->update($table_name, $data_to_update, array('id' => $local_id));
}

function update_entry_with_i_res($response, $local_id){
    // Save to database
    global $wpdb;
    $table_name = $wpdb->prefix . 'cc_donations';

    $data_to_update =  array(
        'i_res' => $response
    );

    $wpdb->update($table_name, $data_to_update, array('id' => $local_id));
}




