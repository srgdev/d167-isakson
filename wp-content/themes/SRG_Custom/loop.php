<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<?php $count = 0; ?>
<?php while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <?php if($count != 0 && ($count % 2 == 0)): ?>
        <br class="clear">
        <hr class="one_line" />
    <?php endif; ?>

    <div class="article_box">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('srg-default'); ?></a>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <p><?php the_excerpt(); ?></p>
        <br/>
        <div class="article_left">
            <a title="Share on Twitter" href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_permalink()); ?>"><i class="fa fa-twitter article_social"></i></a>
            <a title="Share on Facebook" href="javascript:fbShare('<?php echo urlencode(get_permalink()); ?>', '<?php the_title(); ?>', '<?php the_title(); ?>', '<?php echo urlencode($url); ?>', 520, 350)"><i class="fa fa-facebook article_social"></i></a>
            <a title="Comment" href="<?php the_permalink(); ?>"><i class="fa fa-comment article_social"></i></a>
        </div>
        <div class="article_right">
            <p><a href="<?php the_permalink(); ?>" class="article_time"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?></a></p>
        </div>
        <br class="clear"/>
    </div>

    <?php $count++; ?>

<?php endwhile; ?>