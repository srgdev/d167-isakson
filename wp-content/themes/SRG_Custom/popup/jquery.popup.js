$(document).ready(function() {
    //When you click on a link with class of poplight and the href starts with a #
    $('.poplight').click(function(e) {


        var browserWidth = $(window).width();


        e.preventDefault();

        var popID = $(this).attr('popTarget'); //Get Popup Name
        var popWidth = $(this).attr('popWidth'); //Get Popup href to define size
        var popHeight = (popWidth/4)*3; // 4:3 aspect ration
        var popTop = (($(window).height()) - popHeight)/2;

        //Fade in the Popup and add close button for responsive % width
        $('#' + popID).fadeIn().css({ 'max-width': popWidth + 'px' }).css({ 'max-height': popHeight + 'px'}).css({ 'top': popTop + 'px'});
        $('.popup_inner').prepend('<a href="#" class="close btn_close" title="Close Window" alt="Close"></a>');

        //center
        //var browserWidth = $(window).width();
        var popLeft = (browserWidth - popWidth) / 2;
        if (popLeft < 0) {popLeft = 0;}

        $('#' + popID).css({'left' : popLeft + 'px'	});

        //Center on resize
        $(window).resize(function () {
            browserWidth = $(window).width();
            popLeft = (browserWidth - popWidth) / 2;
            if (popLeft < 0) {popLeft = 0;}
            $('#' + popID).css({ 'left' : popLeft + 'px'});
            popNewWidth = $('#' + popID).width();
            popNewHeight = (popNewWidth/4)*3;
            popNewTop = (($(window).height()) - popNewHeight)/2;
            $('#' + popID).css({ 'max-height' : popNewHeight + 'px'});
            $('#' + popID).css({ 'top' : popNewTop + 'px'});
        });

        //Fade in Background
        $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
        $('#fade').fadeIn(); //Fade in the fade layer



        //Close Popups and Fade Layer
        $('a.close, #fade').click(function() {
            var video = $('#' + popID).find('iframe');
            var videoSRC = video.attr('src');
            if(videoSRC != undefined){
                videoSRC = videoSRC.replace('autoplay=1', 'autoplay=0');
            }
            video.attr('src', videoSRC);
            $('#' + popID).remove('iframe');
            $('#fade , .popup_block').fadeOut(function() {
                $('#fade, a.close').remove();  //fade them both out
                setTimeout(function(){$('#' + popID + ' .fluid-width-video-wrapper').append(video);}, 500);
            });
            return false;
        });

    });
});