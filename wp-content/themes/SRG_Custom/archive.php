<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php $url = wp_get_attachment_url( get_post_thumbnail_id(get_option('page_for_posts')) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php
                /* Queue the first post, that way we know
                 * what date we're dealing with (if that is the case).
                 *
                 * We reset this later so we can run the loop
                 * properly with a call to rewind_posts().
                 */
                if ( have_posts() )
                    the_post();
                ?>

                <?php if ( is_day() ) : ?>
                    <?php printf( __( 'Daily Archives: %s', 'twentyten' ), get_the_date() ); ?>
                <?php elseif ( is_month() ) : ?>
                    <?php printf( __( 'Monthly Archives: %s', 'twentyten' ), get_the_date('F Y') ); ?>
                <?php elseif ( is_year() ) : ?>
                    <?php printf( __( 'Yearly Archives: %s', 'twentyten' ), get_the_date('Y') ); ?>
                <?php else : ?>
                    <?php _e( 'Blog Archives', 'twentyten' ); ?>
                <?php endif; ?>

                <?php
                /* Since we called the_post() above, we need to
                 * rewind the loop back to the beginning that way
                 * we can run the loop properly, in full.
                 */
                rewind_posts(); ?></h1>
        </div>
    </div>

    <div class="title_notch">
    </div>

    <div class="interior_content">
        <div class="wrapper">

            <div class="briefing_right">
                <?php get_sidebar(); ?>
            </div>

            <div class="briefing_left">
                <div class="article_left">
                    <h1 class="briefing_header"></h1>
                </div>

                <br class="clear" />

                <?php get_template_part('loop', 'index'); ?>

            </div>
            <br class="clear"/>
            <div class="navigation_bottom">
                <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                    <?php if($wp_query->get('paged') || $wp_query->get('paged') > 1): ?>
                        <p class="margin_top"><a href="<?php previous_posts(); ?>" class="older_tag"><i class="fa fa-angle-left older_tag"></i> &nbsp;&nbsp; newer </a></p>
                    <?php endif; ?>
                    <?php if (($next_url = next_posts($wp_query->max_num_pages, false)) && ($wp_query->get('paged') || $wp_query->get('paged') > 1)): ?>
                        <p class="margin_top older_tag">&nbsp;|&nbsp;</p>
                    <?php endif; ?>
                    <?php if ($next_url = next_posts($wp_query->max_num_pages, false)): ?>
                        <p class="margin_top"><a href="<?php echo $next_url; ?>" class="older_tag">older &nbsp;&nbsp;<i class="fa fa-angle-right older_tag"></i></a></p>
                    <?php endif;?>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>