<?php
/**
 * Template name: Join - Firstclass
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover; background-position: <?php the_field('bg_pos'); ?>;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">
            <div class="full_page">
                <?php the_content(); ?>
                <form class="joinForm" method="post" parsley-validate="true" data-validate="parsley" action="https://dvfirstclass.createsend.com/t/r/s/kyhyhth/">
                    <h3 class="red_text">Volunteer</h3>
                    <input required type="text" name="cm-name" placeholder="Name" class="input_third_left" />
                    <input required type="email" name="cm-kyhyhth-kyhyhth" placeholder="Email" class="input_third_mid" />
                    <input required type="text" name="cm-f-tihdlluy" placeholder="Phone" class="input_third_right" />
                    <input required type="text" name="cm-f-tihdlluj" placeholder="Address" class="input_third_left" />
                    <input required type="text" name="cm-f-tihdllut" placeholder="City" class="input_sixth" />
                    <input required type="text" name="cm-f-tihdllui" placeholder="Zip" class="input_sixth" />
                    <input required type="text" name="cm-f-tihdllud" placeholder="County" class="input_third_right" />
                    <?php if(get_field('issues_section_title')): ?>
                        <h3><?php the_field('issues_section_title'); ?></h3>
                    <?php else: ?>
                        <h3>Which issues are important to you? <span>(check all that apply)</span></h3>
                    <?php endif; ?>
                    <div class="check_mark">
                        <input id="fieldtllrklk-0" name="cm-fo-tihdlrul" value="8278098" type="checkbox" /> <label for="fieldtllrklk-0"><span></span></label><p>Jobs & Economy</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-1" name="cm-fo-tihdlrul" value="8278099" type="checkbox" /> <label for="fieldtllrklk-1"><span></span></label><p>Veterans</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-2" name="cm-fo-tihdlrul" value="8278100" type="checkbox" /> <label for="fieldtllrklk-2"><span></span></label><p>Health Care</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-3" name="cm-fo-tihdlrul" value="8278101" type="checkbox" /> <label for="fieldtllrklk-3"><span></span></label><p>Tax Reform</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-4" name="cm-fo-tihdlrul" value="8278102" type="checkbox" /> <label for="fieldtllrklk-4"><span></span></label><p>Balanced Budget</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-5" name="cm-fo-tihdlrul" value="8278103" type="checkbox" /> <label for="fieldtllrklk-5"><span></span></label><p>Second Amendment</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-6" name="cm-fo-tihdlrul" value="8278104" type="checkbox" /> <label for="fieldtllrklk-6"><span></span></label><p>Border Control</p>
                    </div>
                    <div class="check_mark">
                        <input id="fieldtllrklk-7" name="cm-fo-tihdlrul" value="8278105" type="checkbox" /> <label for="fieldtllrklk-7"><span></span></label><p>Family Issues</p>
                    </div>
                    <br class="clear" />
                    <input type="submit" value="Join the Fight" name="submit" />
                </form>

                <?php if(get_theme_mod('donate')): ?>

                <h3 class="red_text"><?php the_field('donate_title'); ?></h3>
                <p class="join_text"><?php the_field('donate_subtitle'); ?></p>
                <a target="_blank" href="<?php echo get_theme_mod('donate'); ?>"><div class="donate_button">Donate Today</div></a>

                <?php endif; ?>

            </div>
        </div>
    </div>

<?php endwhile; ?>

<script type="text/javascript">  (function() {    window._pa = window._pa || {};    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/569fa18a15af405d370001c8.js";    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);  })();</script>

<script type="text/javascript" src="https://a.adtpix.com/px/?id=107167"></script>

<?php get_footer(); ?>