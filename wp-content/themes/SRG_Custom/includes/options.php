<?php
/**
 * Creates theme options pages, sections, and settings
 */

/*----------------------------------------------------------------------------------------------------*/

/**
 * Wrapper for theme options in srg_theme_options array
 * @param $option_name
 * @return mixed
 */
function get_srg_option($option_name){
    $opts = get_option('srg_theme_options');
    return @$opts[$option_name];
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Echo wrapper for get_srg_option
 * @param $option_name
 */
function srg_option($option_name){
    echo get_srg_option($option_name);
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Adds a submenu page under "Appearance" top level menu.
 */
function add_srg_menu(){
    add_submenu_page( 'themes.php', 'SRG Options', 'SRG Options', 'manage_options', 'srg-options', 'srg_render_page');
}
add_action('admin_menu', 'add_srg_menu');

/*----------------------------------------------------------------------------------------------------*/

/**
 * Callback function to render the options page from above
 */
function srg_render_page()
{
    ?>
    <div class="section panel">
        <h1>SRG Options</h1>
        <?php settings_errors(); ?>
        <form method="post" enctype="multipart/form-data" action="options.php">
            <?php
            settings_fields('srg_theme_options');

            do_settings_sections('srg_theme_options.php');
            ?>
            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>

        </form>
    </div>
    <style>
        .section h3 {
            border-top: 1px solid #ccc;;
            padding-top: 1em;
        }
    </style>
<?php
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Register settings groups and sections for srg_theme_options
 */
function srg_register_settings()
{
    // Register the settings with Validation callback
    register_setting( 'srg_theme_options', 'srg_theme_options', 'srg_validate_options' );

    // Add settings section
    add_settings_section( 'srg_page_options', 'SRG Page Link Options', 'srg_display_page_options', 'srg_theme_options.php' );

    

    // Create pages field
    $field_args_page_thank = array(
        'type'      => 'pages',
        'id'        => 'srg_page_thank',
        'name'      => 'srg_page_thank',
        'desc'      => 'Thankyou Page',
        'std'       => '',
        'label_for' => 'srg_page_thank',
        'class'     => ''
    );

    add_settings_field( 'srg_page_thank', 'Thankyou Page', 'srg_display_dropdown', 'srg_theme_options.php', 'srg_page_options', $field_args_page_thank );

    
}
add_action( 'admin_init', 'srg_register_settings' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Echo some output before the social options sections
 * @param $section
 */
function srg_display_social_options($section){
    echo 'Please enter valid urls.';
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Echo some output before the tracking options sections
 * @param $section
 */
function srg_display_tracking_options($section){
    echo 'Please enter properly formatted &lt;script&gt; tags and &lt;noscript &gt; tags.';
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Echo some output before the tracking options sections
 * @param $section
 */
function srg_display_page_options($section){
    echo 'Select the pages these pointers need to go to';
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Echo output before quote section
 * @param $section
 */
function srg_display_quote_options($section){
    echo 'Enter the quote of the day along with an author and link.';
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Display text fields and textareas for options input
 * @param $args
 */
function srg_display_text($args){
    extract( $args );

    $option_name = 'srg_theme_options';

    $options = get_option( $option_name );

    switch ( $type ) {
        case 'text':
            $options[$id] = @stripslashes($options[$id]);
            $options[$id] = esc_attr( $options[$id]);
            echo "<input class='regular-text$class' type='text' id='$id' name='" . $option_name . "[$id]' value='$options[$id]' />";
            break;
        case 'textarea':
            $options[$id] = @stripslashes($options[$id]);
            $options[$id] = esc_attr( $options[$id]);
            echo "<textarea style='width: 100%;max-width: 25em;height: 10em;' class='regular-text$class' id='$id' name='" . $option_name . "[$id]' >$options[$id]</textarea>";
            break;
    }
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Display pages dropdown select fields
 * @param $args
 */
function srg_display_dropdown($args){
    extract( $args );

    $option_name = 'srg_theme_options';
    $options = get_option( $option_name );

    $options[$id] = @stripslashes($options[$id]);
    $options[$id] = esc_attr( $options[$id]);

    wp_dropdown_pages(array(
        'name' => $option_name . "[$id]",
        'id' => $id,
        'selected' => $options[$id],
        'show_option_none' => 'None'
    ));
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Validate our entire options array upon submission
 *
 * Very subjective about options coming through
 * @param $input
 * @return mixed
 */
function srg_validate_options($input){

    foreach($input as $k => $v)
    {
        $newinput[$k] = trim($v);

        if($k == 'srg_fb' || $k == 'srg_tw' || $k == 'srg_yt' || $k == 'srg_quote_link'){
            if($v != ''){
                // Check the input is a letter or a number
                if(!filter_var($v, FILTER_VALIDATE_URL)) {
                    $newinput[$k] = '';

                    $message = '';

                    switch($k){
                        case 'srg_fb':
                            $message = 'Invalid Facebook URL';
                            break;
                        case 'srg_tw':
                            $message = 'Invalid Twitter URL';
                            break;
                        case 'srg_yt':
                            $message = 'Invalid Youtube URL';
                            break;
                        case 'srg_quote_link':
                            $message = 'Invalid Quote URL';
                            break;
                    }

                    add_settings_error($k, esc_attr( 'settings_updated' ), $message, 'error');
                }
            }

        }
    }

    return $newinput;
}