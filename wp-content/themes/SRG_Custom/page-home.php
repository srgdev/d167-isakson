<?php
/**
 * Template name: Home
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <div class="landing_mid">
        <div class="landing_bg" style="background-image: url('<?php echo $url; ?>'); background-size: cover; background-position: <?php the_field('bg_pos', get_option('page_on_front')); ?>;">
            <div class="black_cover gradient"></div>
            <div class="landing_callout">
                <p class="callout_1"><?php the_field('title1'); ?></p>
                <p class="callout_2 uppercase"><?php the_field('title2'); ?></p>
                <p class="callout_3 uppercase">
                    <?php if(get_field('link1') && get_field('link1_text')): ?><a href="<?php the_field('link1'); ?>"><span class="bluetext"><?php the_field('link1_text'); ?> <?php if(get_field('link2') && get_field('link2_text')): ?>&nbsp;|<?php endif; ?></span></a>&nbsp;<?php endif; ?>
                    <?php if(get_field('link2') && get_field('link2_text')): ?><a target="_blank" href="<?php the_field('link2'); ?>"><span class="redtext"> <?php the_field('link2_text'); ?></span></a><?php endif; ?>
                </p>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>

<!--<!--Popup On Page Load - ID must Be load pop Max width must be set -->-->
<!--<div id="loadPop" class="popup_block" popWidth='830'>-->
<!--	<div class="popup_inner">-->
<!--    	<div class="popHeadline">Find your early voting location</div>-->
<!--        <div class="popMain">-->
<!--        	<form method="post" action="vote-early" >-->
<!--<!--            	<label>Enter the <strong>complete street address</strong> of where you're registered to vote:</label>-->-->
<!--<!--                <input name="address" class="popField" placeholder="Address" type="text" required>-->-->
<!--            	<div class="popFooter">-->
<!--                	<div class="popDates">Early voting October 17<sup>th</sup> -November 4<sup>th</sup></div>-->
<!--                    <input class="popSubmit" type="submit" value="Search">-->
<!--                    <br class="clear">-->
<!--                </div>-->
<!--            </form>-->
<!--        </div>-->
<!--	</div>-->
<!--</div>-->

<!--<script src="--><?php //bloginfo('template_directory'); ?><!--/js/jquery.popup-onload.js" type="text/javascript"></script>-->