<?php
/**
 * Template name: Issues
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover; background-position: <?php the_field('bg_pos'); ?>;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">
            <div class="full_page">
                <?php if(have_rows('issues')) while(have_rows('issues')): the_row(); ?>
                    <div class="issue">
                <h3><?php the_sub_field('title'); ?></h3>
                <?php the_sub_field('content'); ?>
                    <a class="stayInformed" href="#" data-redirect="<?php echo get_tag_link(get_sub_field('redirect_tag')); ?>" data-thankyou="<?php the_sub_field('thankyou'); ?>" data-tag="<?php the_sub_field('tag'); ?>"><?php the_sub_field('buttonText'); ?></a>
                        </div>
                <br/>
                <?php endwhile; ?>

            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>