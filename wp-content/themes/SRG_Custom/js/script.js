function initialize() {


    var input = document.getElementById('searchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);
}

google.maps.event.addDomListener(window, 'load', initialize);



$('.googleapi').submit(function(e){
    var url = "https://www.googleapis.com/civicinfo/v2/voterinfo?electionId=5000&key=AIzaSyD_wIrhcFmt6ACT_WQTuZmTKvVGWLLKEtQ&";
    e.preventDefault();
    url = url + "address="+$('.api_address').val();
    console.log(url);

    var map = new GMaps({
        div: '#map',
        lat: -12.043333,
        lng: -77.028333,
        zoom: 10
    });

    GMaps.geocode({
        address: $('.api_address').val(),
        callback: function(results, status) {
            if (status == 'OK') {
                var latlng = results[0].geometry.location;
                map.setCenter(latlng.lat(), latlng.lng());

            }
        }
    });

    var i = 1;
    var j = 1;

    $.ajax({
        dataType: "json",
        url: url,
        success: function(data){
            console.log(data.earlyVoteSites);
            $.each(data.earlyVoteSites, function( index, value ) {
                addressval = value.address.line1+" "+value.address.city+" "+value.address.state+" "+value.address.zip;






                GMaps.geocode({
                    address: addressval,
                    callback: function(results, status) {
                        if (status == 'OK') {
                            var latlng = results[0].geometry.location;

                            map.addMarker({
                                lat: latlng.lat(),
                                lng: latlng.lng(),
                                icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld="+i+"|FF0000|ffffff",
                                infoWindow: {
                                    content: addressval
                                }
                            });
                            i++;
                            console.log(i);
                        }else{
                            console.log('no');
                        }
                    }
                });

                var result = '<div class="pollLocation"><div class="locationName"><span>'+j+ ".</span> " +addressval + "</div>";
                result += '<div class="timesHeading">Dates & Times:</div> <ul class="times">';
                j++;
                var pollinghours = value.pollingHours.split("\n");

                $.each(pollinghours, function( index, value ) {
                    result += '<li>'+pollinghours[index] + "</li>";

                });

                result +=  "</ul> </div>";

                $('.api_results').append(result);

            });

        }
    });



});







