/*----------------------------------------------------------------------------------------------------*/

/**
 * Global vars
 * @type {boolean}
 */
var flipper = true;

/**
 * Capitalizes a string
 * @param input
 * @returns {string}
 */
function capitalize(input){
    var string = $(input).val();
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Save viewed state on click
 */
function bindSplashLink(){
    if(window.location.href == SRG_Ajax.siteurl + '/splash/'){
        $('.skipLink').click(function(e){
            amplify.store(SRG_Splash.session_key, true);
        });
    }
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Splash page forms
 */
function bindSplashForm(){
    
    var apiUrl = "https://api-platform.i-360.com/1.1/Contacts"; 
    var userId = '5f44c291-edad-4e51-9c8a-96cd04b419a2'; // don't forget to replace with valid user id
    var MyBase64EncodedAuthString = btoa(userId + ':');
    
    $('.splash .joinForm').submit(function(e){
        e.preventDefault();

        var form = $(this);
        
        var name = form.find('input[name="cm-name"]').val().split(' ');
        var email = form.find('input[type=email]').val();
        var tag = form.find('input[name=tag]').val();
        
        if (typeof name[1] == 'undefined') {
            alert('Please enter your full name.');
            return false;
        }

        form.find('.error').remove();
        
        var tagObj = {};
        tagObj.Category = 'Splash';
        tagObj.Tag = tag;
        
        var tagsArray = []
        tagsArray.push(tagObj);
        
        var contact = {};
        
        contact.firstname = name[0];
        contact.lastname = (typeof name[1] != 'undefined' ? name[1] : '');
        contact.Email = email;
        contact.RegisteredState = 'GA';
        contact.State = 'GA';
        contact.Tags = tagsArray;
        
        var contactArray = [];
        contactArray.push(contact);
        
        var importJob = {};
        importJob.Contacts = contactArray;
        importJob.ApiNotification = true;
        
        console.log(importJob);
        
        var json = JSON.stringify(importJob);
        $('#formOverlay').show();
        $.when(sendRequest(json)).then(function(data){
            $('.splash .left, .splash .right').fadeOut(function(){
                    if(redirect != ""){
                        window.location = redirect;
                    }
                    $('#formOverlay').hide();
                    $('.hiddenMessage').fadeIn();
                });
        }, function(){
            $('#formOverlay').hide();
            $('.splash .joinForm').prepend('<span class="error">An error ocurred, please try again.</span>');
        });
    });
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Interactive slider forms
 */
function sliderForm(){

    var form = $('#emailerForm');
    var inputs = form.find('input');
    var position = 0;
    var count = 1;

    $.each(inputs, function(i, v){
        if(i > 0){
            $(v).hide();
        }
        count++;
    });

    $('.joinform_right .tot').html(count);
    $('.joinform_right .num').html(String(position+1));

    $('.joinform_right .fa').click(function(e){
        e.preventDefault();

        var name = form.find('#q1').val().split(' ');

        if($(inputs[position]).parsley('validate')){
            if (typeof name[1] == 'undefined') {
                alert('Please enter your full name.');
                return false;
            } else {
                if(position < (count-2)){
                    $(inputs[position]).slideUp('fast', function(){
                        position ++;
                        $('.joinform_right .num').html(String(position+1));
                        $(inputs[position]).fadeIn();
                    });
                } else {
                    $('#formOverlay').show();
                    $('#emailerForm').submit();
                }
            }
        }
    });

    $('#emailerForm input').keypress(function(event){
        
        var name = form.find('#q1').val().split(' ');
        var keycode = (event.keyCode ? event.keyCode : event.which);
        
        if(keycode == '13'){
            if (typeof name[1] == 'undefined') {
                alert('Please enter your full name.');
                return false;
            } else {
                if($(inputs[position]).parsley('validate')){
                    if(position < (count-2)){
                        $(inputs[position]).slideUp('fast', function(){
                            position ++;
                            $('.joinform_right .num').html(String(position+1));
                            $(inputs[position]).fadeIn();
                            $(inputs[position]).focus();
                        });
                    } else {
                        $('#formOverlay').show();
                        $('#emailerForm').submit();
                    }
                }
            }
        }

    });
    
    form.submit(function(e){
        e.preventDefault();
        
        var name = form.find('#q1').val().split(' ');
        var email = form.find('#q2').val();
        
        if (typeof name[1] == 'undefined') {
            alert('Please enter your full name.');
            return false;
        }

        var tagObj = {};
        tagObj.Category = 'Homepage';
        tagObj.Tag = 'True';
        
        var tagsArray = []
        tagsArray.push(tagObj);
        
        var contact = {};
        
        contact.firstname = name[0];
        contact.lastname = (typeof name[1] != 'undefined' ? name[1] : '');
        contact.Email = email;
        
        contact.RegisteredState = 'GA';
        contact.gender = '';
        contact.Phone = '';
        contact.Address1 = '';
        contact.Address2 = '';
        contact.City = '';
        contact.State = 'GA';
        contact.Zip = '';
        contact.Tags = tagsArray;
        
        var contactArray = [];
        contactArray.push(contact);
        
        var importJob = {};
        importJob.Contacts = contactArray;
        importJob.ApiNotification = true;
        
        var json = JSON.stringify(importJob);
        
        console.log(importJob);

        $('#formOverlay').show();

        var request = sendRequest(json);

        $.when(request).then(function(res){
           window.location.href = SRG_Ajax.thankpage;
        }, function(){
            // Failure
            $('#formOverlay').hide();
            form.parsley('error');
        })
        
        
    });
    
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Send a request to the i360 API
 */
function sendRequest(json){
    var apiUrl = "https://api-platform.i-360.com/2.0/Org/1626/Contacts"; 
    var promise = $.Deferred();
    
    $.when(getToken()).then(function(data){

        var token = data["access_token"];

        $.ajax({
            url: apiUrl,
            type: 'POST',
            contentType: "application/json",
            dataType: 'json',
            data: json,
            crossDomain: false,
            headers: {
                "authorization": 'Bearer ' + token,
                "ContentType": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            },
            success: function(data){
                promise.resolve(data);
            },
            error: function(data){
                promise.reject(data);
            }
        });
    });

    return promise;
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Get i-360 Bearer Token
 */
function getToken() {
    var loginUrl = 'https://login.i-360.com/core/connect/token';
    var username = 'Daniel.coats';
    var password = 'seahawk37';
    var basicAuthHeader = "Basic "+ window.btoa('roclient:secret');
    return $.ajax(
        {
            type: "POST",
            headers: {
                "authorization": basicAuthHeader,
                "ContentType": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            },
            url: loginUrl,
            data: {
                    'grant_type': 'password',
                    'username': username,
                    'password': password,
                    'scope': 'openid profile sampleApi'
            },
            contentType: "application/x-www-form-urlencoded",
            accepts: "application/json",
            dataType: "json"
        });
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Facebook popup
 * @param url
 * @param title
 * @param descr
 * @param image
 * @param winWidth
 * @param winHeight
 */
function fbShare(url, title, descr, image, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}

/*----------------------------------------------------------------------------------------------------*/

/**
 * Avoid `console` errors in browsers that lack a console.
 */
(function() {
    var method;
    var noop = function noop() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

/*----------------------------------------------------------------------------------------------------*/

/**
 * Determines whether or not to bounce to a splash page
 */
function showHideSplash(){

    var splashUrl = SRG_Splash.splash_page_url;
    var session_seen_splash = SRG_Splash.session_seen_flash;
    var session_key = SRG_Splash.session_key;

    if(splashUrl != false){
        if(window.location.href != splashUrl && window.location.href == (SRG_Ajax.siteurl + '/')){
            if(!amplify.store(session_key) && !session_seen_splash){
                window.location.href = splashUrl;
            } else {
                $('body').fadeIn(150);
            }
        } else if(window.location.href == splashUrl){
            amplify.store(session_key, true);
            $('body').fadeIn(150);
        } else {
            $('body').fadeIn(150);
        }
    } else {
        $('body').fadeIn(150);
    }
}

/*----------------------------------------------------------------------------------------------------*/

$(document).ready(function(){

    // Form bindings and splash page links
    showHideSplash();
    sliderForm();
    bindSplashForm();
    bindSplashLink();

    $('.interior_content').fitVids();

    /**
     * Capitalize entries
     */
    $('input[name=cm-name]').change(function(){
        $(this).val(capitalize(this));
    });

    $('input[name=cm-name]').keyup(function(){
        $(this).val(capitalize(this));
    });

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Mobile menu
     */
    $('.mobmenuBtn').click(function(){
        $('.mobmenubody').slideToggle()
    });

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Post tools Dropdown
     */
	$('.toolBtn').click(function() {
    	if ($(this).hasClass("active")) {
			$(this).removeClass('active').siblings('.toolDrop').slideUp(200);
		} else {
			$(this).addClass('active').siblings('.toolDrop').slideDown(200);
		}
	});

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Searh box dropdown
     */
    $("#hider").click(function(e){
        e.preventDefault();

        var hideBox = $("#hideBox");

        if (flipper == true) {
            hideBox.fadeIn();
            flipper = false;
            document.getElementById('s').focus();
        } else {
            hideBox.fadeOut();
            flipper = true;
        }
    });

    /*----------------------------------------------------------------------------------------------------*/


    /**
     * Form submit (stay informed)
     */
    $('.stayInformed').click(function(e){

        e.preventDefault();
        
        var apiUrl = "https://api-platform.i-360.com/1.1/Contacts"; 
        var userId = '5f44c291-edad-4e51-9c8a-96cd04b419a2'; // don't forget to replace with valid user id
        var MyBase64EncodedAuthString = btoa(userId + ':');

        var value = $(this).data('tag');
        var tyMessage = $(this).data('thankyou');
        var redirect = $(this).data('redirect');

        $(this).before('<form style="width:0px;"><input name="email" type="email" placeholder="email address" /><input type="hidden" name="tag" value="'+value+'" /></form>');

         $(this).parent().find('input[type=email]').on('blur', function() {
              $(this).mailcheck({
                suggested: function(element, suggestion){
                    $('.suggestion').remove();
                    $(element).after('<span class="suggestion">Did you mean '+suggestion.full+'?</span>');
                },
                empty: function(element){
                    $('.suggestion').remove();
                }
              });
    });

        $(this).parent().find('form').css('width', '200px').submit(function(e){

            e.preventDefault();

            if($(this).parsley('validate')){

                var form = $(this);
                var email = form.find('input[name=email]').val();
                var tag = form.find('input[name=tag]').val();

                var tagObj = {};
                tagObj.Category = 'Issue';
                tagObj.Tag = tag;

                var tagObj2 = {};
                tagObj2.Category = 'IssuePage';
                tagObj2.Tag = 'True';
                
                var tagsArray = []
                tagsArray.push(tagObj);
                tagsArray.push(tagObj2);
                
                var contact = {};
                contact.Email = email;
                contact.Tags = tagsArray;
                contact.RegisteredState = 'GA';
                contact.State = 'GA';
                
                var contactArray = [];
                contactArray.push(contact);
                
                var importJob = {};
                importJob.Contacts = contactArray;
                importJob.ApiNotification = true;
                
                var json = JSON.stringify(importJob);

                $('#formOverlay').show();

                $.when(sendRequest(json)).then(function(data){
                    $('#formOverlay').hide();
                   form.parent().find('form, a').slideUp(400);
                        setTimeout(function(){
                            form.parent().append('<h3 class="ty">'+tyMessage+'</h3>');
                        }, 401);
                        setTimeout(function(){
                            window.location.href = redirect;
                        }, 2000);
                }, function(){
                    $('#formOverlay').hide();
                    form.find('input[type=email]').parsley('error');
                });
            }

        });

        $(this).unbind('click');
        $(this).click(function(e){
            e.preventDefault();
            $(this).parent().find('form').submit();
        });
        
    });

    /*----------------------------------------------------------------------------------------------------*/

    /**
     * Donation Amounts
     */
    $('#panel1 .amount').click(function() {
        $('#panel1 .amount').removeClass('active');
        $(this).addClass('active');
        $( "#otherAmount").removeClass('parsley-error').val('$' + $(this).data('amount'));
    });

    /**
     * REGEXP pattern patching only numbers, then prepends $
     */
    $( "#otherAmount" ).blur(function() {
        $(this).removeClass('parsley-error');
        var val = $(this).val();
        val = val.replace(/\D/g, '');
        if(val != '' && (val.slice(0, 1) != '$')){
            val = '$' + val;
        }
        $(this).val(val);
    });

    /**
     * NEXT BUTTON 1
     * Checks values on first panel for validity, then goes to page 2
     */
    $('#panel1 .nextBtn').click(function() {

        $('#panel1 input, #panel1 select').removeClass('parsley-error');

        var amount = $('#otherAmount').val();
        var freq = $('#frequency').val();

        if(!amount || amount == ''){
            $('#otherAmount').addClass('parsley-error');
        }

        if(!freq || freq == ''){
            $('#frequency').addClass('parsley-error');
        }

        if(freq && amount){
            $('#donateFormBox').removeClass('step1 step3').addClass('step2');
        }
    });

    /**
     * PREV BUTTON 1
     * Allows user to visit panel 1 for changes
     */
    $('#panel2 .prevBtn').click(function(){
        $('#donateFormBox').removeClass('step2 step3').addClass('step1');
    });

    /**
     * NEXT BUTTON 2
     * Checks user information (name, add, etc) before continuing
     */
    $('#panel2 .nextBtn').click(function() {

        var valid = true;

        $('#panel2 input').each(function(i, v){
            if($(v).parsley('validate') === false){
                valid = false;
            };
        });

        $('#panel2 select').each(function(i, v){
            if($(v).parsley('validate') === false){
                valid = false;
            };
        });

        if(valid){
            $('#donateFormBox').removeClass('step2 step1').addClass('step3');
        }

    });

    /**
     * PREV BUTTON 2
     * Allows user to visit panel 2 for changes
     */
    $('#panel3 .prevBtn').click(function(){
        $('#donateFormBox').removeClass('step1 step3').addClass('step2');
    });

    /**
     * SUBMIT DONATION BUTTON
     */
    var in_progress = false;
    $('#donateForm').submit(function(e) {

        // Prevent normal submit
        e.preventDefault();

        // If the form is not already in submission
        if(!in_progress){

            // Get form, start checking data
            var form = $(this);

            // Get error container
            var error_cont = $('.error_container');
            error_cont.text('').slideUp();

            // Check amount
            var amount = form.find('#otherAmount').val();

            // Remove dollar sign if present
            if(amount.indexOf('$') > -1){
                amount = amount.slice(1)
            }
            form.find('#otherAmount').val(amount);

            // Get the data, start validating (assume true, then validate)
            var data = form.serialize();
            var valid = true;

            // Panel 3
            $('#panel3 input').each(function(i, v){
                if($(v).parsley('validate') === false){
                    valid = false;
                };
            });

            $('#panel3 select').each(function(i, v){
                if($(v).parsley('validate') === false){
                    valid = false;
                };
            });

            // If all good
            if(valid){

                // We are now in progress - disable and fade the form
                in_progress = true;
                form.find('input, select').attr('disabled', true);
                form.addClass('fade');
                $('html,body').css('cursor','wait');

                // Send the data
                $.post(SRG_Ajax.ajaxurl, data, function(data){

                    // No longer in progress
                    form.removeClass('fade');
                    form.find('input, select').attr('disabled', false);
                    in_progress = false;
                    $('html,body').css('cursor','default');

                    // Try to parse a json response
                    var json = '';
                    try {
                        json = $.parseJSON(data);

                        if(json.code == 200){
                            $('.donateHeader').fadeOut();
                            $('.donateSteps').fadeOut();
                            $('.formPanel').fadeOut();
                            $('.certifyBlock').fadeOut();
                            $('.donate_content_area').fadeOut(function(){
                                $('#donateRow').addClass('thank');
                            });



                        } else {
                            // Error message here
                            error_cont.text('Error: '+json.message).slideDown();
                            alert(json.message);
                        }

                    } catch(e){
                        json = data;
                        alert('There was an error in processing your information on the server: ' + json);
                    }
                    console.log(json);
                }).fail(function(xhr){
                    // No longer in progress
                    form.removeClass('fade');
                    form.find('input, select').attr('disabled', false);
                    in_progress = false;
                    $('html,body').css('cursor','default');
                    alert('There was an error in processing your donation on the server: ' + xhr.responseText);
                });
            }
        }

    });
    
    $('#join_page_form').submit(function(e){
        
        e.preventDefault();
        
        var apiUrl = "https://api-platform.i-360.com/1.1/Contacts"; 
        var userId = '5f44c291-edad-4e51-9c8a-96cd04b419a2'; // don't forget to replace with valid user id
        var MyBase64EncodedAuthString = btoa(userId + ':');
        
        if($(this).parsley('validate')){

            var form = $(this);
            var email = form.find('input[name=email]').val();
            var name = form.find('input[name=name]').val().split(' ');
            var tags = [];
            
            if (typeof name[1] == 'undefined') {
                alert('Please enter your full name.');
                return false;
            }
            
            var tagsArray = []
            form.find('input[name=tags]:checked').each(function(i, v){
                tagsArray.push({
                    'Category' : 'Issue',
                    'Tag' : $(v).val()
                });
                
            });

            // Tag them as volunteer
            tagsArray.push({
                'Category' : 'Volunteer',
                'Tag' : 'True'
            });
            
            var contact = {};
            contact.RegisteredState = 'GA';
            contact.firstname = name[0];
            contact.lastname = name[1];
            contact.Phone = $('input[name=phone]').val();
            contact.Email = email;
            contact.Address1 = $('input[name=address]').val();
            contact.City = $('input[name=city]').val();
            contact.State = 'GA';
            contact.Zip = $('input[name=zip]').val();
            contact.Tags = tagsArray;
            
            var contactArray = [];
            contactArray.push(contact);
            
            var importJob = {};
            importJob.Contacts = contactArray;
            importJob.ApiNotification = true;
            
            console.log(importJob);
            
            var json = JSON.stringify(importJob);

            $('#formOverlay').show();

            $.when(sendRequest(json)).then(function(data){
                window.location.href = SRG_Ajax.thankpage;
            }, function(){
                $('#formOverlay').hide();
                alert('There was an error processing your request.  Please check your information and try again.');
            })
        }

    });

    // Size iframe container on donate embed page
    if($('.embedded_donate').length > 0){

        var donation_height = $('.certifyBlock').outerHeight();

        donation_height = donation_height + 120;

        $('#donateFormBox').css('height', donation_height + 'px');

    }


    /*----------------------------------------------------------------------------------------------------*/

    $('input[type=email]').on('blur', function() {
      $(this).mailcheck({
        suggested: function(element, suggestion){
            $('.suggestion').remove();
            $(element).after('<span class="suggestion">Did you mean '+suggestion.full+'?</span>');
        },
        empty: function(element){
            $('.suggestion').remove();
        }
      });
    });

}); // END document ready

