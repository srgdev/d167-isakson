$(document).ready(function() {
	if ($('body').hasClass('home')) {
		var videoSrc = $(".popup_inner iframe").attr("src");
		
		$('html, body').animate({
			scrollTop: $('body').offset().top
		}, 250);
		
		//center
		var browserWidth = $(window).width();
		var popWidth = $('#loadPop').attr('popWidth');
		var popLeft = (browserWidth - popWidth) / 2; 
		if (popLeft < 0) {popLeft = 0;}
		
		$('#loadPop').css({'left' : popLeft + 'px'	});
		
		//Center on resize
		$(window).resize(function () { 
			browserWidth = $(window).width();
			popLeft = (browserWidth - popWidth) / 2; 
			if (popLeft < 0) {popLeft = 0;} 
			$('#loadPop').css({ 'left' : popLeft + 'px'});
		});
			
		//Fade in the Popup and add close button
		$('#loadPop').fadeIn().css({ 'max-width': popWidth + 'px' });
		$('.popup_inner').prepend('<a href="#" class="close btn_close" title="Close Window" alt="Close"></a>');
		
		//Fade in Background
		$('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
		$('#fade').fadeIn(); //Fade in the fade layer
	
		//Close Popups and Fade Layer and stop video
		$('a.close, #fade').click(function() {
			$('#fade , .popup_block').fadeOut(function() {
				$('#fade, a.close').remove();  //fade them both out
			});
			$(".popup_inner iframe").attr("src","");
			return false;
		});
	}
});
