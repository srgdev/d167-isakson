<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover; background-position: <?php the_field('bg_pos'); ?>;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php the_title(); ?><br/>
                <?php $tags = wp_get_post_tags($post->ID); ?>
                <?php if($tags) foreach ($tags as $tag): ?>
                    <a href="<?php echo get_tag_link($tag->term_id); ?>" class="keyword_tag"><?php echo $tag->name; ?></a>
                <?php endforeach; ?>
            </h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">

            <div class="detail_left">
                <?php the_content(); ?>

                <div class="detail_article_left">
                    <a href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_permalink()); ?>"><i class="fa fa-twitter detail_article_social"></i></a>
                    <a href="javascript:fbShare('<?php echo urlencode(get_permalink()); ?>', '<?php the_title(); ?>', '<?php the_title(); ?>', '<?php echo urlencode($url); ?>', 520, 350)"><i class="fa fa-facebook detail_article_social"></i></a>
                    <a href="<?php the_permalink(); ?>"><i class="fa fa-comment detail_article_social"></i></a>
                </div>
                <div class="detail_article_right">
                    <p><a href="" class="detail_article_time"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?></a></p>
                </div>
                <br class="clear"/>

                <hr />

                <?php if(false): ?>
                <div class="comments">
                    <i class="fa fa-comment comments_icon"></i>
                    <div class="comments_count">
                        <p><?php comments_number( 0, 1, '%' ); ?> </p>
                    </div>
                    <div class="comments_title">
                        <p>Comments</p>
                    </div>
                    <br class="clear" />

                    <?php $comments = get_comments(array('post_id'=> $post->ID)); ?>

                    <?php if($comments) foreach ($comments as $comment): ?>

                        <?php if($comment->comment_parent == 0): ?>

                            <div class="comments_left">
                                <?php echo get_avatar( $comment ); ?>
                            </div>
                            <div class="comments_right" id="comment-<?php echo $comment->comment_ID; ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/comments_left_pointer.png" class="left_pointer" />
                                <div class="comments_name">
                                    <h3><?php the_author_meta( 'user_nicename', $comment->user_id ); ?>  <span><?php comment_date( 'F j, Y \a\t g:i a', $comment->comment_ID ); ?></span></h3>

                                </div>
                                <div class="comments_button">
                                    <?php $default = array(
                                        'add_below'  => 'comment',
                                        'respond_id' => 'respond',
                                        'reply_text' => __('Reply'),
                                        'login_text' => __('Log in to Reply'),
                                        'depth'      => 1,
                                        'before'     => '',
                                        'after'      => '',
                                        'max_depth'  => 2
                                    ); ?>
                                    <?php comment_reply_link( $default, $comment->comment_ID); ?>
                                </div>
                                <br class="clear"/>
                                <?php comment_text( $comment->comment_ID ); ?>

                                <?php foreach($comments as $subComment): ?>

                                    <?php if($subComment->comment_parent == $comment->comment_ID): ?>
                                    <div class="comments_right" id="comment-<?php echo $subComment->comment_ID; ?>">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/comments_left_pointer.png" class="left_pointer" />
                                        <div class="comments_name">
                                            <h3><?php the_author_meta( 'user_nicename', $subComment->user_id ); ?>  <span><?php comment_date( 'F j, Y \a\t g:i a', $subComment->comment_ID ); ?></span></h3>

                                        </div>
                                        <br class="clear"/>
                                        <?php comment_text( $subComment->comment_ID ); ?>
                                    </div>
                                    <?php endif; ?>

                                <?php endforeach; ?>

                            </div>

                        <?php endif; ?>

                    <?php endforeach; ?>

                    <?php comment_form(); ?>

                </div>
                 <?php endif; ?>
            </div>
           


            <div class="detail_right">
                <?php get_sidebar(); ?>
            </div>
            <br class="clear"/>

        </div>
    </div>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>