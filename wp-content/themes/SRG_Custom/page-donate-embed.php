<?php
/**
 * Template name: Donate Embedded
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php if(!get_field('header_image_donate')): ?>
        <div class="interior_banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/bg-banner_donate.jpg); background-size: cover; background-position: center center">
            <div class="black_cover gradient"></div>
        </div>
    <?php else :?>
        <div class="interior_banner" style="background-image: url(<?php the_field('header_image_donate'); ?>); background-size: cover; background-position: center center">
            <div class="black_cover gradient"></div>
        </div>
    <?php endif; ?>

    <div id="donateRow" class="embedded_donate">
        <div class="rowInner">
            <div id="donateTop">
                <div id="donateFormBox" class="step1">
                    <div class="shadow"></div>
                    <iframe id="donateForm" src='<?php the_field('embed_link'); ?>'></iframe>
                </div>

                <div class="certifyBlock">
                    <?php the_content(); ?>
                </div>

                <br class="clear"/>
            </div><!-- End donate top -->

            <div class="donate_content_area">
            </div>


        </div>
    </div>

    <br class="clear"/>

<?php endwhile; ?>

<?php get_footer(); ?>