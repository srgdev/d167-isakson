<?php
/**
 * Template name: Splash Page
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php wp_title( '|', true, 'right' );?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link href='http://fonts.googleapis.com/css?family=Lato:400,100italic,300,100,700,900,700italic,300italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) )
            wp_enqueue_script( 'comment-reply' );
        ?>
        <?php wp_head(); ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/amplify.min.js"></script>

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-76x76.png">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#da532c">

        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
        <![endif]-->

    </head>

<body <?php body_class('splash'); ?>>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="landing_mid">
        <div class="landing_bg" style="background-image: url('<?php echo $url; ?>'); background-size: auto 100%; background-position: <?php the_field('bg_pos', get_option('page_on_front')); ?>;background-size: cover;">
            <div class="black_cover gradient"></div>
            <div class="landing_callout">
                <div class="hiddenMessage">
                    <p class="callout_1"><?php the_field('thankyou_message'); ?></p>
                    <script type="text/javascript" charset="utf-8">
                        var redirect = "<?php the_field('thank_you_redirect'); ?>";
                    </script>
                    <!-- <a class="skiplink" href="<?php bloginfo('url'); ?>"><button>Continue to site</button></a> -->
                </div>
                <div class="left">
                <p class="callout_1"><?php the_field('text1'); ?></p>
                <p class="callout_2 uppercase"><?php the_field('text2'); ?></p>
                <p class="callout_3 uppercase"><?php the_field('text3'); ?> </p>
                    </div>
                <div class="right">
                    <form class="joinForm" parsley-validate="true" data-validate="parsley" method="post">
                        <input type="text" name="cm-name" placeholder="Name" required />
                        <input type="email" name="email" placeholder="Email Address" required />
                        <input type="hidden" name="tag" value="<?php the_field('tag'); ?>" />
                        <input type="submit" value="Add my name" />
                    </form>

                </div>
                <br class="clear" />
                <a class="skipLink" href="<?php bloginfo('url'); ?>">skip to main site <i class="fa fa-arrow-right"></i></a>
                <div class="splash_content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>