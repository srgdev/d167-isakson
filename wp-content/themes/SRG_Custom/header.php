<?php
/**
 * The template for displaying the header.
 * Opens our page and calls wp_head to add js and other options
 * @uses wp_head()
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />

    <title><?php wp_title( '|', true, 'right' );?></title>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBlZymmUK0J4nWC16GVvbgb7MihG3jK9G8"></script>

    <?php if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );
    ?>
    <?php wp_head(); ?>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-76x76.png">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#da532c">

    <!--[if lt IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->
    
</head>

<body <?php body_class(); ?> style="display:none;">

<header>
    <div class="wrapper">
        <a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sig_white_new.png" class="top_sig_logo"/></a>
        <ul class="top_social">
            <?php if(get_theme_mod('twitter')): ?><li><a target="_blank" href="<?php echo get_theme_mod('twitter'); ?>"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
            <?php if(get_theme_mod('facebook')): ?><li><a target="_blank" href="<?php echo get_theme_mod('facebook'); ?>"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
            <?php if(get_theme_mod('youtube')): ?><li><a target="_blank" href="<?php echo get_theme_mod('youtube'); ?>"><i class="fa fa-youtube"></i></a></li><?php endif; ?>
            <li>
                <div id="hideBox">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/search_triangle.png" />
                    <form action="<?php bloginfo('url'); ?>">
                        <input type="text" name="s" id="s" placeholder="Search . . ." />
                        <input type="submit" value="GO" />
                    </form>
                </div>
                <a href="#" id="hider"><i class="fa fa-search"></i></a>
            </li>
        </ul>
        <div class="mobmenuBtn"><i class="fa fa-bars"></i></div>
        <?php srg_nav_menu('header'); ?>
        <br class="clear" />

        <div class="mobmenu">
            
            <div class="mobmenubody">
					<?php wp_nav_menu(array('menu' => 'Main Menu')); ?>
            </div>
        </div>
        
    </div>
</header>
