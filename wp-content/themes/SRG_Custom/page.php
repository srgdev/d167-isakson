<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover; background-position: <?php the_field('bg_pos'); ?>;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">
            <div>
                <?php the_content(); ?>
            </div>
            <br>
            <br class="clear"/>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>