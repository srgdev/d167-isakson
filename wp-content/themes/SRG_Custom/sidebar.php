<?php
/**
 * The Sidebar containing widget areas.
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<form action="<?php bloginfo('url'); ?>">
    <input name="s" type="text" placeholder="" />
</form>
<div class="sidebar_tags">
    <h5>tags</h5>
    <hr class="two_lines" />
    <ul>
        <?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'post_tag')); ?>
    </ul>
</div>
<div class="sidebar_tags">
    <h5>archives</h5>
    <hr class="two_lines" />
    <ul>
        <?php wp_get_archives(); ?>
    </ul>
</div>
<div class="sidebar_tags">
    <h5>categories</h5>
    <hr class="two_lines" />
    <ul>
        <?php wp_list_categories(array('title_li' => '',)); ?>
    </ul>
</div>
<br class="clear" />