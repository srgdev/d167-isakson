<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
get_header(); ?>

    <?php $url = wp_get_attachment_url( get_post_thumbnail_id(get_option('page_for_posts')) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php _e( 'Not Found', 'srg' ); ?></h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">
            <div>
                <?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'srg' ); ?>
            </div>
            <br>
            <br class="clear"/>
        </div>
    </div>

<?php get_footer(); ?>