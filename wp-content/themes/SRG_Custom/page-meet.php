<?php
/**
 * Template name: Meet
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover; background-position: <?php the_field('bg_pos'); ?>;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">
            <div class="content_left">
               <?php the_content(); ?>
            </div>
            <div class="content_right">
                <?php if(have_rows('images')): ?>
                    <?php $i = 0; while(have_rows('images')): the_row();?>
                        <?php if($i < 3): ?>
                            <img src="<?php the_sub_field('image'); ?>" />
                            <?php $i++; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
                <br/>
                <p><div class="photo poplight" popTarget="mainSliderWrap" popWidth="400"><a href="#">SEE MORE &nbsp;></a></div></p>
            </div>
            <br class="clear"/>
        </div>
    </div>

    <!-- BIG SLIDER -->
    <div id="mainSliderWrap" class="popup_block">
        <div class="popup_inner"></div>
        <div id="mainSlider" class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="0" data-cycle-slides=".photo" data-cycle-prev=".prev" data-cycle-next=".next" data-cycle-caption=".counterWrap" data-cycle-caption-template='<span class="counter">{{slideNum}}</span> OF <span id="total">{{slideCount}}</span>'>
            <?php while(have_rows('images')): the_row();?>
                <div class="photo" style="background:url(<?php the_sub_field('image'); ?>) no-repeat center; background-size:cover;"></div>
            <?php endwhile; ?>
            <div class="controls">
                <div class="counterWrap"></div>
                                <span class="mainControlNav">
                                    <a class="prev"><i class="fa fa-chevron-circle-left"></i></a>
                                    <a class="next"><i class="fa fa-chevron-circle-right"></i></a>
                                </span>
                <br class="clear" />
            </div>
            <br class="clear" />
        </div>
    </div>
    <!-- /BIG SLIDER -->

<?php endwhile; ?>

<?php get_footer(); ?>