<?php
/**
 * Template name: Early Voting
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php global $post; $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

    <div class="interior_banner" style="background-image: url(<?php echo $url; ?>); background-size: cover; background-position: <?php the_field('bg_pos'); ?>;">
        <div class="black_cover gradient"></div>
    </div>

    <div class="interior_pagetitle">
        <div class="wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>

    <div class="title_notch" />
    </div>

    <div class="interior_content">
        <div class="wrapper">
            <div class="full_page voteEarly_page">
                <h3 class="red_text">Early Voting Locations</h3>
                <br>
                <div class="pollSearch">
                	<label>Enter the <strong>complete street address</strong> of where you're registered to vote:</label>
                    <form method="post" action="#" class="googleapi">
                        <input name="address" id="searchTextField" class="popField api_address left" placeholder="Address" type="text" required>
                        <input class="popSubmit" type="submit" value="Submit">
                     	<br class="clear">
                    </form>
                </div>
                <br>
                <div class="pollMap" id="map" style="width:100%; height: 500px;"></div>
                
                <div class="pollLocations api_results">

                </div>

            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>