<?php
/**
 *
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <?php if(!get_field('header_image_donate')): ?>
        <div class="interior_banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/bg-banner_donate.jpg); background-size: cover; background-position: center center">
            <div class="black_cover gradient"></div>
        </div>
    <?php else :?>
        <div class="interior_banner" style="background-image: url(<?php the_field('header_image_donate'); ?>); background-size: cover; background-position: center center">
            <div class="black_cover gradient"></div>
        </div>
    <?php endif; ?>

    <div id="donateRow">
        <div class="rowInner">
            <div id="donateTop">

                <form id="donateForm">

                    <div id="donateFormBox" class="step1">
                        <div class="donateHeader">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/cards_2.png">
                            <div class="title"><?php the_field('title'); ?></div>
                            <div class="subtitle"><?php the_field('subtitle'); ?></div>
                            <br class="clear"/>
                        </div>
                        <div class="donateSteps">
                            <div class="progressBar">
                                <span></span>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/donate-steps.png">
                            </div>
                            <div class="labels">
                                <label>Amount</label><label>Information</label><label>Payment</label>
                            </div>
                        </div>

                        <div id="panel1" class="formPanel">
                            <div class="sectionTitle">SELECT AN AMOUNT:</div>
                            <div class="amounts">
                                <div class="amount" data-amount="10">$10</div>
                                <div class="amount" data-amount="25">$25</div>
                                <div class="amount" data-amount="50">$50</div>
                                <div class="amount" data-amount="100">$100</div>
                                <div class="amount" data-amount="250">$250</div>
                                <div class="amount" data-amount="500">$500</div>
                                <div class="amount" data-amount="1000">$1000</div>
                                <div class="amount" data-amount="2700">$2700</div>
                                <input name="amount" id="otherAmount" type="text" placeholder="Other" required>
                            </div>
                            <br><br>
                            <div class="sectionTitle">DONATION FREQUENCY:</div>
                            <div class="frequencySelect">
                                <label for="frequency"><i class="fa fa-angle-down"></i></label>
                                <select name="frequency" id="frequency" required>
                                    <option value="O">One-time</option>
                                    <option value="M">Monthly (until October 2016)</option>
                                </select>
                            </div>
                            <br><br>
                            <div class="donateFormFooter">
                                <div class="nextBtn">Next</div>
                                <br class="clear"/>
                            </div>
                        </div> <!-- End Panel1 -->

                        <div id="panel2" class="formPanel">

                            <div class="fieldBox">
                                <label class="sectionTitle">First name*</label>
                                <input name="fname" class="field" type="text" required>
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">last name*</label>
                                <input name="lname" class="field" type="text" required>
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">Email*</label>
                                <input name="email" class="field" type="email" required>
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">phone*</label>
                                <input name="phone" class="field" type="text" required pattern="^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$">
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">Address 1*</label>
                                <input name="address1" class="field" type="text" required>
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">Address 2</label>
                                <input name="address2" class="field" type="text" >
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">City*</label>
                                <input name="city" class="field" type="text" required>
                            </div>
                            <div class="fieldBox state">
                                <label class="sectionTitle">State*</label>
                                <select name="state" class="field" required >
                                    <option value="">Select</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </div>
                            <div class="fieldBox zip">
                                <label class="sectionTitle">zip*</label>
                                <input name="zip" class="field" type="text" required pattern="^\d{5}$">
                            </div>
                            <br class="clear"/>

                            <div class="donateFormFooter">
                                <label class="sectionTitle">*REQUIRED FIELD</label>
                                <br class="clear" />
                                <div class="prevBtn">Back</div>
                                <div class="nextBtn">Next</div>
                                <br class="clear"/>
                            </div>
                        </div> <!-- End Panel2 -->


                        <div id="panel3" class="formPanel">

                            <div class="fieldBox ccnum">
                                <label class="sectionTitle">Card Number*</label>
                                <input name="ccnum" class="field" type="text" required pattern="^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|6(?:011|5[0-9]{2})[0-9]{12})$">
                            </div>

                            <div class="fieldBox third">
                                <label class="sectionTitle">month*</label>
                                <select name="exp_month" class="field" required>
                                    <option value="" selected>Month</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>

                            <div class="fieldBox third">
                                <label class="sectionTitle">year*</label>
                                <select name="exp_year" class="field" required>
                                    <option value="" selected>Year</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                    <option value="2029">2029</option>
                                    <option value="2030">2030</option>
                                </select>
                            </div>

                            <div class="fieldBox third">
                                <label class="sectionTitle">cvc*</label>
                                <input name="cvc" class="field" type="text" required pattern="^[0-9]{3,4}$">
                            </div>

                            <br class="clear"/><br>
                            <label class="sectionTitle red">Federal law requires us to collect the following information:</label>
                            <br><br><br>

                            <div class="fieldBox">
                                <label class="sectionTitle">occupation*</label>
                                <input name="occupation" class="field" type="text" required>
                            </div>

                            <div class="fieldBox">
                                <label class="sectionTitle">employer*</label>
                                <input name="employer" class="field" type="text" required>
                            </div>
                            <br class="clear"/>

                            <div class="donateFormFooter">
                                <label class="sectionTitle">*REQUIRED FIELD</label>
                                <br class="clear" />
                                <div class="prevBtn">Back</div>

                                <input type="hidden" name="action" value="srg_donate" />
                                <?php wp_nonce_field( 'srg_donate'); ?>
                                <?php if(isset($_GET['ref'])): ?>
                                    <input type="hidden" name="source" value="<?php echo $_GET['ref']; ?>" />
                                <?php endif; ?>
                                <input type="submit" value="DONATE" class="nextBtn">
                                <br class="clear"/>
                                <div class="error_container">

                                </div>
                            </div>
                        </div> <!-- End Panel3 -->

                        <div class="thankyouMessage">
                            <?php the_field('thankyou_message'); ?>
                        </div>

                    </div> <!-- End donateformbox -->

                </form>

                <div class="certifyBlock">
                    <?php the_field('content_area_1'); ?>
                </div>

                <br class="clear"/>
            </div><!-- End donate top -->

            <div class="donate_content_area">
                <?php the_field('content_area_2'); ?>
            </div>


        </div>
    </div>

    <br class="clear"/>

<?php endwhile; ?>

<?php get_footer(); ?>