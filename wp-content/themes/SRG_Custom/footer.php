<?php
/**
 * The template for displaying the footer.
 * Closes our page and calls wp_footer() to add js and other options
 * @uses wp_footer()
 * @package WordPress
 * @subpackage SRG
 * @since 1.0
 */
?>
<div class="joinform_bottom">
    <img src="<?php echo get_template_directory_uri(); ?>/images/blue_chevrons_down.png" class="mid_chevrons" />
    <div class="joinform_left">
        <p>Get the latest from Johnny</p>
    </div>
    <div class="joinform_right">
        <form id="emailerForm" method="post">
            <input id="q1" name="name" type="text"  placeholder="What is your name?" required/>
            <input id="q2" name="email" type="email" placeholder="What is your e-mail?" required />
        </form>
        <p><span class="num">1</span> / <span class="tot">3</span>&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></p>
    </div>
    <br class="clear"/>
</div>

<footer>
    <div class="wrapper">
        <!--<img src="<?php //echo get_template_directory_uri(); ?>/images/sig_blue.png" class="footsig"/>-->
        <!--<h2>Johnny Isakson. United States Senate.</h2>-->
       
        <div class="disclaimer">
            <p>Paid for by Georgians for Isakson<br />770-818-1493</p>
        </div>
       <!-- <div class="foot-left">
            <?php //if(get_theme_mod('presskit_file')): ?>
                <p><a href="<?php //echo get_theme_mod('presskit_file'); ?>"><i class="fa fa-file-text-o"></i> &nbsp;download a press kit</a></p>
            <?php //endif; ?>
        </div>-->
        <div class="foot-mid">
            <?php srg_nav_menu('footer'); ?>
        </div>
        <!--<div class="foot-right"><a href="http://stoneridgegroup.com"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_srg.png" /></a></div>-->
        <br class="clear" />
    </div>
</footer>
<script src="<?php echo get_template_directory_uri(); ?>/js/gmaps.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fitvids.js"></script>
<script>$(".respVideo").fitVids();</script>

<div id="googleBox">
<script type="text/javascript">window.acxm={api:Function(),x:function(){var k=document,e=encodeURIComponent,r="script",m=k.getElementsByTagName(r)[0],i=k.createElement(r),t="&c=";try{t=t+k.cookie.match('_acxm=([^;]*)').slice(1);}catch(z){};i.async=true;i.src="https://t.acxiom-online.com/tag/f98a083eb5d09e02afb5fc619930a2e8/st.js?l="+e(k.location)+"&r="+e(k.referrer)+t+"&v=2.0&z="+Math.random();m.parentNode.insertBefore(i,m)}()}</script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55188456-1', 'auto');
ga('require', 'displayfeatures');
ga('send', 'pageview');
</script>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970442984;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>

<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/970442984/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript" src="https://a.adtpix.com/px/?id=107166"></script>
</div>

<div id="formOverlay"></div>

<?php wp_footer(); ?>
</body>
</html>